## Getting set up

poetry-install:
    poetry install

pre-commit:
    pre-commit install


## Starting the server

start-django:
    poetry run python manage.py runserver


## Testing the endpoint

http-get:
    http get http://127.0.0.1:8000/num_to_english\?number\=1200000

http-post:
    http --form post http://127.0.0.1:8000/num_to_english number="154"

http-get-not-integer:
    http get http://127.0.0.1:8000/num_to_english\?number\=what

http-get-none:
    http get http://127.0.0.1:8000/num_to_english

http-post-none:
    http post http://127.0.0.1:8000/num_to_english

http-get-too-big:
    http get http://127.0.0.1:8000/num_to_english\?number\=1000000000
