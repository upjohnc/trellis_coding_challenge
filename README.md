# Upjohn - Trellis Coding Challenge

This is coding challenge for Trellis.  It is a simple http endpoint that takes in either a `get` or `post` request
with a `number` value.  The endpoint's response is the number translated into its English equivalent.

## Usage

There is a justfile that has basic "recipes" to run and test the endpoint.  The app `just` works like
`make`.  You can get it from homebrew : `brew install just`.  Run recipes like : `just poetry-install`

To start you will want to create the virtualenv.  This project uses poetry.  You can create the virtualenv
and install it with `just poetry-install`.

To hit the endpoints, you will need to start the server locally.  In a terminal type `just start-django`.
In a separate terminal you then test hitting the endpoint with the just recipes.  For instance
`just http-get` will send a get command with a basic integer.

## Notes on Design

To be upfront, I made use of the tutorial.  So there is a level of "reverse engineered" in getting
the DRF set up for the creation of the endpoint.

My thinking behind the design was to have the "view" function handle the input from the call
and then the response.  I then tried to make the function that converts to English easier to update.
I did try to break out the integer into millions, thousands, hundreds so that it was more composable allowing
the function to be more easily refactored

I don't have tests around the function.  Adding tests for refactoring would be a good next step.

### English Translation

The function was designed to do return the count of a particular digit.  For instance, `22` is not `twenty two`
but rather `two tens two` - of the tens digit there are two and then two of the singles digit.
I figured that by breaking the integer down into composed parts that an engineer can refactor the handling
of particular digits such as changing `three tens` to `thirty` or `one tens two` to `twelve`.
