from typing import List

from rest_framework.decorators import api_view
from rest_framework.response import Response


class NotAnInteger(Exception):
    pass


class IntegerTooBig(Exception):
    pass


singles_digit = {
    "1": "one",
    "2": "two",
    "3": "three",
    "4": "four",
    "5": "five",
    "6": "six",
    "7": "seven",
    "8": "eight",
    "9": "nine",
}


def convert_hundreds(hundreds_string: str) -> List:
    hundreds_english = []
    if len(hundreds_string) == 3:
        digit = singles_digit.get(hundreds_string[-3])
        if digit is not None:
            hundreds_english.extend([digit, "hundred"])
    if len(hundreds_string) >= 2:
        digit = singles_digit.get(hundreds_string[-2])
        if digit is not None:
            hundreds_english.extend([digit, "tens"])
    if len(hundreds_string) >= 1:
        digit = singles_digit.get(hundreds_string[-1])
        if digit is not None:
            hundreds_english.extend([digit])
    return hundreds_english


def convert_integer_to_english(integer: int) -> str:
    try:
        integer = int(integer)
    except (ValueError, TypeError):
        raise NotAnInteger("Value needs to be an integer")

    if integer > (1_000_000_000 - 1):
        raise IntegerTooBig("Value needs to less than a billion")

    integer_as_string = str(integer)

    hundreds = integer_as_string[-3:]
    hundreds_converted = convert_hundreds(hundreds)

    thousands = integer_as_string[-6:-3]
    thousands_converted = []
    if len(thousands) > 0:
        thousands_converted = convert_hundreds(thousands) + ["thousand"]

    millions = integer_as_string[-9:-6]
    millions_converted = []
    if len(millions) > 0:
        millions_converted = convert_hundreds(millions) + ["million"]

    return " ".join(millions_converted + thousands_converted + hundreds_converted)


@api_view(["GET", "POST"])
def endpoint_num_to_english(request):
    def get_english(number_input):
        try:
            english = convert_integer_to_english(number_input)
        except (NotAnInteger, IntegerTooBig):
            return Response({"status": "not_ok"})

        return Response({"status": "ok", "num_in_english": english})

    if request.method == "GET":
        number = request.query_params.get("number")
        return get_english(number)

    if request.method == "POST":
        number = request.data.get("number")
        return get_english(number)
